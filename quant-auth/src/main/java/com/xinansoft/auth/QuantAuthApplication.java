package com.xinansoft.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 认证服务器启动类
 *
 * @author Quant
 * @date 2019/3/20 16:27
 */
@SpringBootApplication(scanBasePackages = "com.xinansoft")
public class QuantAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuantAuthApplication.class, args);
    }
}

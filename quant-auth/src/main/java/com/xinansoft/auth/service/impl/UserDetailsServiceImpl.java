package com.xinansoft.auth.service.impl;

import com.xinansoft.core.common.util.CollectionUtil;
import com.xinansoft.core.common.util.StringUtil;
import com.xinansoft.upms.model.SysUser;
import com.xinansoft.upms.service.SysUserService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * 身份认证实现类
 *
 * @author Quant
 * @date 2019/3/18 14:08
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Reference(version = "1.0")
    private SysUserService sysUserService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        final SysUser user = sysUserService.getByUserName(username);

        if (user != null) {
            Set<GrantedAuthority> authorities = CollectionUtil.newHashSet();
            if (CollectionUtil.isNotEmpty(user.getPerms())) {
                user.getPerms().forEach(permisson -> authorities.add(new SimpleGrantedAuthority(permisson)));
            }
            if (CollectionUtil.isNotEmpty(user.getRoles())) {
                user.getRoles().forEach(role -> authorities.add(new SimpleGrantedAuthority(StringUtil.join("_", "ROLE", role.toUpperCase()))));
            }
            boolean enabled = user.getStatus() == 1 ? true : false;
            // 构造security用户
            return new User(username, user.getPassword(), enabled, true, true, true, authorities);
        }

        return null;
    }
}

package com.xinansoft.auth.controller;

import com.xinansoft.core.common.ui.Result;
import com.xinansoft.upms.model.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Objects;

/**
 * @author Quant
 * @date 2019/3/18 17:34
 */
@RestController
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

  /*  @RequestMapping(value = "/token", method = RequestMethod.POST)
    public Result createAuthenticationToken(@RequestBody SysUser sysUser, HttpServletRequest request) {
        authenticate(sysUser.getUserName(), sysUser.getPassword());

        HttpSession session = request.getSession(false);

        return Result.success("成功",session.getId());
    }

    *//**
     * Authenticates the user. If something is wrong, an {@link AuthenticationException} will be thrown
     *//*
    private void authenticate(String username, String password) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new DisabledException("User is disabled!", e);
        } catch (BadCredentialsException e) {
            throw new BadCredentialsException("Bad credentials!", e);
        }
    }*/
}

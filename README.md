quant-platform 快速开发平台


# 技术选型


基础框架：Spring Boot 2.1.3.RELEASE

安全框架：Spring Security

分布式SESSION管理：Spring Session

远程调用：Dubbo

持久层框架：Mybatis-plus

数据库连接池：阿里巴巴Druid

缓存框架：redis

日志打印：logback

其他：fastjson，swagger-ui，lombok，hutool等等。
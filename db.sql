
-- 用户表
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '编号',
  `user_name` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `real_name` varchar(50) NOT NULL COMMENT '真实姓名',
  `admin` tinyint(1) UNSIGNED DEFAULT NULL COMMENT ' 是否管理员 0：否 1：是',
  `sex` tinyint(1) UNSIGNED DEFAULT NULL COMMENT '性别 1：男 2：女',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '手机号',
  `status` tinyint(1) UNSIGNED DEFAULT 1 COMMENT '状态(0:禁止,1:正常)',
  `create_by` bigint(20) UNSIGNED DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) UNSIGNED DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `order_num` mediumint UNSIGNED DEFAULT NULL COMMENT '排序',
  `description` varchar(1000) DEFAULT NULL COMMENT '备注',
  `delete_flag` tinyint(1) UNSIGNED DEFAULT 0 COMMENT '删除状态(0:正常,1:删除)',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '用户表';


-- 角色表
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '编号',
  `parent_id` bigint(20) UNSIGNED NULL COMMENT '父角色编号',
  `name` varchar(50) NOT NULL COMMENT '用户名',
  `code` varchar(50) DEFAULT NULL COMMENT '角色编码',
  `app_code` varchar(50) DEFAULT NULL COMMENT '应用码',
  `create_by` bigint(20) UNSIGNED DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) UNSIGNED DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `order_num` mediumint UNSIGNED DEFAULT NULL COMMENT '排序',
  `description` varchar(1000) DEFAULT NULL COMMENT '备注',
  `delete_flag` tinyint(1) UNSIGNED DEFAULT 0 COMMENT '删除状态(0:正常,1:删除)',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '角色表';

-- 菜单表
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '编号',
  `parent_id` bigint(20) UNSIGNED NULL COMMENT '父菜单编号',
  `name` varchar(50) NOT NULL COMMENT '用户名',
  `perms` varchar(50) DEFAULT NULL COMMENT '权限标识',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单地址',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单iconClass',
  `type` tinyint(1) UNSIGNED DEFAULT 0 COMMENT '类型（0：目录，1：菜单，2：按钮）',
  `status` tinyint(1) UNSIGNED DEFAULT 1 COMMENT '状态(0:禁止,1:正常)',
  `path` varchar(500) DEFAULT NULL COMMENT '层级路径',
  `app_code` varchar(50) DEFAULT NULL COMMENT '应用码',
  `create_by` bigint(20) UNSIGNED DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(20) UNSIGNED DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `order_num` mediumint UNSIGNED DEFAULT NULL COMMENT '排序',
  `description` varchar(1000) DEFAULT NULL COMMENT '备注',
  `delete_flag` tinyint(1) UNSIGNED DEFAULT 0 COMMENT '删除状态(0:正常,1:删除)',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '菜单表';

-- 角色菜单关联表
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '编号',
  `user_id` bigint(20) UNSIGNED NOT NULL COMMENT '用户编号',
  `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '角色编号',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '用户角色关联表';


DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '编号',
  `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '角色编号',
  `menu_id` bigint(20) UNSIGNED NOT NULL COMMENT '菜单编号',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COMMENT = '角色菜单关联表';


-- 系统日志
CREATE TABLE `sys_log` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '编号',
  `userName` varchar(50) COMMENT '用户名',
  `operation` varchar(50) COMMENT '用户操作',
  `method` varchar(200) COMMENT '请求方法',
  `params` varchar(5000) COMMENT '请求参数',
  `time` bigint NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) COMMENT 'IP地址',
  `create_date` datetime COMMENT '创建时间',
  `delete_flag` tinyint(1) UNSIGNED DEFAULT 0 COMMENT '删除状态(0:正常,1:删除)',
  PRIMARY KEY (`id`)
) ENGINE=`InnoDB` DEFAULT CHARACTER SET utf8 COMMENT='系统日志';

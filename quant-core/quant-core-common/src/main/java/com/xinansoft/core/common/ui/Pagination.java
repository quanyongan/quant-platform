package com.xinansoft.core.common.ui;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import lombok.Data;

/**
 * 分页工具类
 *
 * @author Quant
 * @date 2019/3/6 9:49
 */
@Data
public class Pagination implements Serializable {

    private static final long serialVersionUID = 2129146631422640332L;
    /**
     * 当前分页总页数(一共有多少页)
     */
    private Long pages = 0L;

    /**
     * 当前第几页
     */
    private Long current = 1L;
    /**
     * 总数
     */
    private Long total = 0L;
    /**
     * 每页显示条数，默认 10
     */
    private Long size = 10L;

    /**
     * 查询数据列表
     */
    private Collection<?> records = Collections.emptyList();

    /**
     * 排序条件
     */
    private Map<String, String> orderBy;
    /**
     * 查询参数，pojo or map
     */
    private Map<String, Object> search = Collections.emptyMap();

    /**
     * 构造函数
     *
     * @param pages
     * @param total
     * @param records
     */
    public Pagination(Long pages, Long total, Collection<?> records) {
        this.pages = pages;
        this.total = total;
        this.records = records;
    }

}

package com.xinansoft.core.common.ui;

import java.io.Serializable;

import cn.hutool.http.HttpStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 定义公共的响应对象
 *
 * @author Quant
 * @date 2019/3/6 9:56
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result implements Serializable {

    private static final long serialVersionUID = -2399389249304492038L;

    /**
     * 状态码
     */
    private Integer code;

    /**
     * 是否成功
     */
    private boolean success = true;
    /**
     * 消息
     */
    private String msg;
    /**
     * 返回的数据对象
     */
    private Object data;

    public static Result error() {
        return ok(false, HttpStatus.HTTP_INTERNAL_ERROR, "未知异常，请联系管理员", null);
    }

    public static Result error(String msg) {
        return ok(false, HttpStatus.HTTP_INTERNAL_ERROR, msg, null);
    }

    public static Result error(Object data) {
        return ok(false, HttpStatus.HTTP_INTERNAL_ERROR, "error", data);
    }

    public static Result error(String msg, Object data) {
        return ok(false, HttpStatus.HTTP_INTERNAL_ERROR, msg, data);
    }

    public static Result success() {
        return ok(true, HttpStatus.HTTP_OK, "success", null);
    }

    public static Result success(String msg) {
        return ok(true, HttpStatus.HTTP_OK, msg, null);
    }

    public static Result success(Object data) {
        return ok(true, HttpStatus.HTTP_OK, "success", data);
    }

    public static Result success(String msg, Object data) {
        return ok(true, HttpStatus.HTTP_OK, msg, data);
    }

    public static Result ok(boolean success, Integer code, String msg) {
        return ok(success, code, msg, null);
    }

    public static Result ok(boolean success, Integer code, String msg, Object data) {
        Result r = new Result();
        r.setSuccess(success);
        r.setCode(code);
        r.setMsg(msg);
        r.setData(data);
        return r;
    }
}

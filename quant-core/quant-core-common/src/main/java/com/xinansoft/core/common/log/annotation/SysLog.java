package com.xinansoft.core.common.log.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 系统日志注解
 *
 * @author Quant
 * @date 2019/3/20 14:17
 */
@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@Target({java.lang.annotation.ElementType.METHOD})
@Documented
@Inherited
public @interface SysLog {

    /**
     * 日志内容
     *
     * @return
     */
    String value();

}

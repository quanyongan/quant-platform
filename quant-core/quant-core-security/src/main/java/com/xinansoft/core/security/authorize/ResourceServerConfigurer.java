package com.xinansoft.core.security.authorize;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;

/**
 * 资源服务器管理配置
 *
 * @author Quant
 * @date 2019/3/18 16:46
 */
public interface ResourceServerConfigurer {

    void configure(HttpSecurity http) throws Exception;
}

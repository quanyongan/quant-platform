package com.xinansoft.core.security.filter;

import com.alibaba.fastjson.JSON;
import com.xinansoft.core.security.bean.LoginRequest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 重写支持Ajax登录逻辑
 *
 * @author Quant
 * @date 2019/3/22 10:11
 */
public class QuantAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    public QuantAuthenticationFilter(String defaultFilterProcessesUrl) {
        super(defaultFilterProcessesUrl);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        if ((MediaType.APPLICATION_JSON_UTF8_VALUE.equals(request.getContentType()) || MediaType.APPLICATION_JSON_VALUE.equals(request.getContentType())) && HttpMethod.POST.toString().equals(request.getMethod())) {
            LoginRequest loginRequest = JSON.parseObject(request.getInputStream(), LoginRequest.class);
            UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(loginRequest.getUserName(), loginRequest.getPassword());
            return this.getAuthenticationManager().authenticate(authRequest);
        }
        return null;
    }

}

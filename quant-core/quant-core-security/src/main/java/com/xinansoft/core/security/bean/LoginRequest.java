package com.xinansoft.core.security.bean;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 登录请求参数
 *
 * @author Quant
 * @date 2019/3/22 10:13
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequest implements Serializable {

    private static final long serialVersionUID = -6951963577167681326L;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 密码
     */
    private String password;
}

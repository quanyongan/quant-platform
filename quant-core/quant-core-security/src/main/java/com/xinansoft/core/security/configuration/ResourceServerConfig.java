
package com.xinansoft.core.security.configuration;

import cn.hutool.core.util.StrUtil;
import com.xinansoft.core.security.authorize.ResourceServerConfigurer;
import com.xinansoft.core.security.handler.QuantAccessDeniedHandler;
import com.xinansoft.core.security.properties.SecurityProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.access.AccessDeniedHandler;

import java.util.Collections;
import java.util.List;

/**
 * 资源服务器配置
 *
 * @author Quant
 * @date 2019/3/18 11:35
 */
@Order(0)
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceServerConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private SecurityProperties securityProperties;

    /**
     * 资源服务器配置列表
     */
    private List<ResourceServerConfigurer> configurers = Collections.emptyList();

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .exceptionHandling().accessDeniedHandler(accessDeniedHandler())
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS).permitAll()
                .antMatchers("/oauth/token", "/druid/**").permitAll()
                .antMatchers(getPermitUrls()).permitAll();
        http.headers().frameOptions().sameOrigin().cacheControl();
        //循环多个配置器配置
        for (ResourceServerConfigurer configurer : configurers) {
            configurer.configure(http);
        }
        //什麽都沒有配置的话，加上所有连接都要授权
        if (configurers.isEmpty()) {
            http.authorizeRequests().anyRequest().authenticated();
        }
    }


    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.OPTIONS)
                .antMatchers(
                        "/*.html",
                        "/favicon.ico",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js",
                        "/v2/**",
                        "/swagger-ui.html",
                        "/swagger-resources/**",
                        "/webjars/**"
                );
    }

    /**
     * 自定义访问拒绝
     *
     * @return AccessDeniedHandler
     */
    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        return new QuantAccessDeniedHandler();
    }

    @Autowired(required = false)
    public void setConfigurers(List<ResourceServerConfigurer> configurers) {
        this.configurers = configurers;
    }

    /**
     * 获取用户自定义配置的允许通过的地址
     *
     * @return String[]
     */
    public String[] getPermitUrls() {
        String permitUrls = securityProperties.getPermitUrls();
        if (StrUtil.isNotEmpty(permitUrls)) {
            return StrUtil.splitToArray(permitUrls, ',');
        }
        return new String[]{};
    }
}
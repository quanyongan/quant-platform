package com.xinansoft.core.security.handler;

import cn.hutool.http.HttpStatus;
import com.alibaba.fastjson.JSON;
import com.xinansoft.core.common.ui.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 当无权限访问时，自定义处理
 *
 * @author Quant
 * @date 2019/3/18 11:39
 */
@Slf4j
public class QuantAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException {
        log.info("处理权限异常. e={}", e);
        response.setStatus(HttpStatus.HTTP_OK);
        response.setContentType("application/json;charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");
        response.getWriter().write(JSON.toJSONString(Result.ok(false, HttpStatus.HTTP_UNAUTHORIZED, "无访问权限。", e.getMessage())));


    }
}
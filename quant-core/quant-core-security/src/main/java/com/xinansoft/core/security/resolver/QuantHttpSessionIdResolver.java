package com.xinansoft.core.security.resolver;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;
import org.springframework.session.web.http.HttpSessionIdResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

/**
 * 自定义 Session ID 解决策略,同事支持 Cookie和header
 *
 * @author Quant
 * @date 2019/3/11 17:36
 */
public class QuantHttpSessionIdResolver implements HttpSessionIdResolver {

    private static final String WRITTEN_SESSION_ID_ATTR = QuantHttpSessionIdResolver.class.getName().concat(".WRITTEN_SESSION_ID_ATTR");
    private CookieSerializer cookieSerializer = new DefaultCookieSerializer();

    private static final String HEADER_X_AUTH_TOKEN = "x-token";

    private String headerName;

    @Override
    public List<String> resolveSessionIds(HttpServletRequest httpServletRequest) {
        //cookie方式
        List<String> sessionIds = this.cookieSerializer.readCookieValues(httpServletRequest);
        //cookie方式如果找不到，那么则采用header方式获取
        if (CollUtil.isEmpty(sessionIds)) {
            String headerValue = httpServletRequest.getHeader(HEADER_X_AUTH_TOKEN);
            sessionIds = headerValue != null ? Collections.singletonList(headerValue) : Collections.emptyList();
        }
        return sessionIds;
    }

    @Override
    public void setSessionId(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, String sessionId) {
        if (!sessionId.equals(httpServletRequest.getAttribute(WRITTEN_SESSION_ID_ATTR))) {
            httpServletRequest.setAttribute(WRITTEN_SESSION_ID_ATTR, sessionId);
            this.cookieSerializer.writeCookieValue(new CookieSerializer.CookieValue(httpServletRequest, httpServletResponse, sessionId));
        }

        httpServletResponse.setHeader(HEADER_X_AUTH_TOKEN, sessionId);
    }

    @Override
    public void expireSession(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        this.cookieSerializer.writeCookieValue(new CookieSerializer.CookieValue(httpServletRequest, httpServletResponse, ""));
        httpServletResponse.setHeader(this.headerName, "");
    }

    /**
     * 支持自定义cookie配置
     *
     * @param cookieSerializer
     */
    public void setCookieSerializer(CookieSerializer cookieSerializer) {
        if (cookieSerializer == null) {
            throw new IllegalArgumentException("cookieSerializer cannot be null");
        } else {
            this.cookieSerializer = cookieSerializer;
        }
    }

    /**
     * 自定义Header的名字
     *
     * @param headerName
     * @return
     */
    public void setHeaderName(String headerName) {
        if (StrUtil.isEmpty(headerName)) {
            this.headerName = HEADER_X_AUTH_TOKEN;
        } else {
            this.headerName = headerName;
        }
    }
}

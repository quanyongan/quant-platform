package com.xinansoft.core.security.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Security配置类
 *
 * @author Quant
 * @date 2019/3/18 11:10
 */
@Configuration
@ConfigurationProperties(prefix = "quant.security")
public class SecurityProperties {

    /**
     * 身份认证Url地址
     */
    private String authUrl = "/oauth/token";

    /**
     * 无需权限即可访问的url
     */
    private String permitUrls;

    /**
     * Spring Session通过头部传递session id的参数名
     */
    private String tokenName = "x-token";

    public String getAuthUrl() {
        return authUrl;
    }

    public void setAuthUrl(String authUrl) {
        this.authUrl = authUrl;
    }

    public String getPermitUrls() {
        return permitUrls;
    }

    public void setPermitUrls(String permitUrls) {
        this.permitUrls = permitUrls;
    }

    public String getTokenName() {
        return tokenName;
    }

    public void setTokenName(String tokenName) {
        this.tokenName = tokenName;
    }
}

package com.xinansoft.core.mybatis.base;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import cn.hutool.core.collection.CollectionUtil;
import com.xinansoft.core.common.ui.Pagination;
import com.xinansoft.core.mybatis.support.WrapperHelper;
import org.springframework.transaction.annotation.Transactional;

/**
 * 基础服务接口实现
 *
 * @author Quant
 * @date 2018-11-09 10:03:58
 */
public class BaseServiceImpl<M extends BaseMapper<E>, E extends BaseModel<E>> extends ServiceImpl<M, E> implements BaseService<E> {
    /**
     * 分页获取数据
     */
    @Override
    @Transactional(readOnly = true, rollbackFor = {Exception.class})
    public Pagination getPagination(Pagination pagination) {
        IPage<E> p = new Page<E>(pagination.getCurrent(), pagination.getSize());
        p = page(p, WrapperHelper.buildWrapper(currentModelClass(), pagination.getSearch(), pagination.getOrderBy()));
        return new Pagination(p.getPages(), p.getTotal(), p.getRecords());
    }

    /**
     * 验证是否已经存在
     */
    @Override
    @Transactional(readOnly = true, rollbackFor = {Exception.class})
    public boolean validate(E entity) {
        List<E> list = list(new QueryWrapper<E>(entity));
        if (CollectionUtil.isNotEmpty(list)) {
            return false;
        }
        return true;
    }

}

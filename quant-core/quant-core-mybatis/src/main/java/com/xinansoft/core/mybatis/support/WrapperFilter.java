package com.xinansoft.core.mybatis.support;


import com.xinansoft.core.common.util.StringUtil;

/**
 * mybatis-plus 条件构造过滤器
 * 
 * @author Quant
 * @date 2018-11-09 10:00:50
 */
public class WrapperFilter {

	/** 多个属性间OR关系的分隔符. */
	public static final String OR_SEPARATOR = "_or_";

	/** 属性比较类型. */
	public enum MatchType {
		// 等于
		EQ,
		// 不等于
		NE,
		// 模糊查询
		LIKE,
		// 小于
		LT,
		// 大于
		GT,
		// 小于等于
		LE,
		// 大于等于
		GE;
	}

	private MatchType matchType = null;
	private String matchValue = null;
	private String[] propertyNames = null;

	public WrapperFilter() {

	}

	/**
	 * @param filterName
	 *            比较属性字符串,含待比较的比较类型、属性值类型及属性列表. eg. like_name_or_login_name
	 * @param value
	 *            待比较的值.
	 */
	public WrapperFilter(final String filterName, final String value) {
		String matchTypeCode = StringUtil.subBefore(filterName, "_", false);

		// 如果没有条件参数，那么使用 “等于”
		if (StringUtil.isEmpty(matchTypeCode)) {
			matchTypeCode = "eq";
		}
		try {
			this.matchType = Enum.valueOf(MatchType.class, StringUtil.upperCase(matchTypeCode));
		} catch (RuntimeException e) {
			this.matchType = null;
		}
		String propertyNameStr = StringUtil.subAfter(filterName, "_", false);
		// 如果没有具体参数，那么直接使用filterName
		if (StringUtil.isEmpty(propertyNameStr)) {
			propertyNameStr = filterName;
		}
		this.propertyNames = StringUtil.splitByWholeSeparator(propertyNameStr, OR_SEPARATOR);
		// 如果没有具体参数，那么propertyNames直接自己创建一个
		if (propertyNames == null) {
			propertyNames = new String[] { propertyNameStr };
		}
		this.matchValue = value;
	}

	public MatchType getMatchType() {
		return matchType;
	}

	public void setMatchType(MatchType matchType) {
		this.matchType = matchType;
	}

	public String getMatchValue() {
		return matchValue;
	}

	public void setMatchValue(String matchValue) {
		this.matchValue = matchValue;
	}

	public String[] getPropertyNames() {
		return propertyNames;
	}

	public void setPropertyNames(String[] propertyNames) {
		this.propertyNames = propertyNames;
	}

}

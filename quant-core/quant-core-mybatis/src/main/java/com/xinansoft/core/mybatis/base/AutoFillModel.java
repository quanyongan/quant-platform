package com.xinansoft.core.mybatis.base;

import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;

import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Getter;
import lombok.Setter;


/**
 * 自动填充公共基类
 *
 * @author Quant
 * @date 2019/3/21 11:59
 */
@Getter
@Setter
public class AutoFillModel<T extends BaseModel<T>> extends BaseModel<T> {
    private static final long serialVersionUID = 5204478195800350480L;
    /**
     * 创建人
     **/
    @TableField(value = "create_by", fill = FieldFill.INSERT)
    private Long createBy;
    /**
     * 创建时间
     **/
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /**
     * 更新人
     **/
    @TableField(value = "update_by", fill = FieldFill.UPDATE)
    private Long updateBy;
    /**
     * 更新时间
     **/
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    /**
     * 排序
     **/
    @TableField(value = "order_num", fill = FieldFill.INSERT)
    private Integer orderNum;

    /**
     * 逻辑删除标识
     **/
    @TableLogic
    @TableField(value = "delete_flag")
    private Integer deleted;

}

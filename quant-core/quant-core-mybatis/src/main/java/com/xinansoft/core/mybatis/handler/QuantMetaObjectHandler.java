package com.xinansoft.core.mybatis.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;

import java.sql.Timestamp;

/**
 * mybatis-plus自定义填充公共字段
 *
 * @author Quant
 * @date 2019/3/21 14:41
 */
public class QuantMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        // 设置创建人，需要从登录上下问中获取 TODO
        setFieldValByName("createBy", 1L, metaObject);
        // 创建时间
        setFieldValByName("createTime", new Timestamp(System.currentTimeMillis()), metaObject);
        // 排序参数
        Object orderNumValue = getFieldValByName("orderNum", metaObject);
        if (orderNumValue == null) {
            // 默认都是1，如果设置了，那就不进行设置
            setFieldValByName("orderNum", 1, metaObject);
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        // 更新人，需要从登录上下问中获取 TODO
        setFieldValByName("updateBy", 1L, metaObject);
        // 更新时间
        setFieldValByName("updateTime", new Timestamp(System.currentTimeMillis()), metaObject);

    }
}




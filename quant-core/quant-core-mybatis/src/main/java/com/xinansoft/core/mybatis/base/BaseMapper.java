package com.xinansoft.core.mybatis.base;

/**
 * 公共Mapper接口
 *
 * @author Quant
 * @date 2019/3/6 9:59
 */
public interface BaseMapper<T extends BaseModel> extends com.baomidou.mybatisplus.core.mapper.BaseMapper<T> {

}

package com.xinansoft.core.mybatis.base;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xinansoft.core.common.ui.Pagination;
import org.springframework.transaction.annotation.Transactional;

/**
 * 服务基础接口
 *
 * @author Quant
 * @date 2018-11-09 10:03:31
 */
public interface BaseService<E extends BaseModel<E>> extends IService<E> {

    /**
     * 查询分页数据
     *
     * @param pagination 分页对象
     * @return Pagination
     */
    @Transactional(readOnly = true, rollbackFor = {Exception.class})
    Pagination getPagination(Pagination pagination);

    /**
     * 验证数据是否已经存在
     *
     * @param entity 实体
     * @return
     */
    @Transactional(readOnly = true, rollbackFor = {Exception.class})
    boolean validate(E entity);
}

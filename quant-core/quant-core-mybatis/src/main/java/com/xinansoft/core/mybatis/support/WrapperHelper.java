package com.xinansoft.core.mybatis.support;

import java.lang.reflect.Field;
import java.util.Map;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.xinansoft.core.common.util.StringUtil;

/**
 * 条件构造器辅助类
 * 
 * @author Quant
 * @date 2018-11-09 10:00:42
 */
public class WrapperHelper {

	/**
	 * 单表操作==>条件构造器辅助类
	 *
	 * @param <T>
	 * @param searchMap
	 *            查询参数 格式：like_name_or_login_name
	 * @param orderBy
	 *            排序参数,格式：userName:asc,userId:desc
	 * @param searchMap
	 * @return
	 */
	@SuppressWarnings("unused")
	public static <T> QueryWrapper<T> buildWrapper(Class<T> clazz, Map<String, Object> searchMap, Map<String, String> orderBy) {

		QueryWrapper<T> qw = new QueryWrapper<T>();
		TableField annotation = null;
		Field field = null;
		// 1、先过滤带有查询条件的参数
		Map<String, Object> conditionMap = filterConditionMap(searchMap);
		// 2、查询条件构建 eg. like_name_or_login_name
		if (CollUtil.isNotEmpty(conditionMap)) {
			WrapperFilter filter = null;
			int index = 0;
			boolean isExist;
			String column = null;
			for (Map.Entry<String, Object> entry : conditionMap.entrySet()) {
				isExist = true;
				filter = new WrapperFilter(entry.getKey(), String.valueOf(entry.getValue()));
				// 每次重新赋值
				index = 0;
				for (String propertyName : filter.getPropertyNames()) {
					field = ReflectUtil.getField(clazz, propertyName);
					if (field == null) {
						isExist = false;
						continue;
					}
					annotation = field.getAnnotation(TableField.class);
					if (annotation != null) {
						column = annotation.value();
					} else {
						// 如果找不到注解，那采用驼峰装下划线的方式
						column = StringUtil.toUnderlineCase(propertyName);
					}
					// 不是开始和结束字段
					if (index != 0 && index != filter.getPropertyNames().length) {
						qw.or();
					}
					switch (filter.getMatchType()) {
					case EQ:
						qw.eq(column, filter.getMatchValue());
						break;
					case NE:
						qw.ne(column, filter.getMatchValue());
						break;
					case LIKE:
						qw.like(column, String.valueOf(filter.getMatchValue()));
						break;
					case LT:
						qw.lt(column, filter.getMatchValue());
						break;
					case GT:
						qw.gt(column, filter.getMatchValue());
						break;
					case LE:
						qw.le(column, filter.getMatchValue());
						break;
					case GE:
						qw.ge(column, filter.getMatchValue());
						break;
					default:
						break;
					}
					// 自增，如果小于参数的长度
					index++;
				}
			}
		}
		// 2、构 排序语句 eg. "userName:asc,userId:desc"
		if (MapUtil.isNotEmpty(orderBy)) {
			for (Map.Entry<String, String> entry : orderBy.entrySet()) {
				field = ReflectUtil.getField(clazz, entry.getKey());
				if (field != null) {
					annotation = field.getAnnotation(TableField.class);
					if (annotation != null) {
						if ("asc".equals(StringUtil.lowerCase(entry.getValue()))) {
							qw.orderByAsc(annotation.value());
						} else if ("desc".equals(StringUtil.lowerCase(entry.getValue()))) {
							qw.orderByDesc(annotation.value());
						}
					}
				}
			}
		}
		return qw;
	}

	/**
	 * 多表构建查询排序条件
	 *
	 * @param <T>
	 * @param mappingColumn
	 *            映射类 ,格式：username=>s.ID,status=>s.STATUS
	 * @param searchMap
	 *            查询参数 格式：like_name_or_login_name
	 * @param orderBy
	 *            排序参数,格式：userName:asc,userId:desc
	 * @return 条件参数
	 */
	public static <T> QueryWrapper<T> buildWrapper(String mappingColumn, Map<String, Object> searchMap, Map<String, String> orderBy) {
		// 1、先将映射构建成key-value的方式
		Map<String, String> columnMap = CollectionUtil.newHashMap();
		if (StrUtil.isNotEmpty(mappingColumn)) {
			// 格式"username=>s.ID,status=>s.STATUS"
			String[] columns = StrUtil.split(mappingColumn, ",");
			String[] colAry = null;
			for (String col : columns) {
				// 格式"username=>s.ID
				colAry = StrUtil.split(col, "=>");
				if (colAry != null && colAry.length == 2) {
					columnMap.put(colAry[0], colAry[1]);
				}
			}
		}
		// 查询条件构建 eg. like_name_or_login_name
		QueryWrapper<T> qw = new QueryWrapper<T>();
		buildSearchQueryWrapper(qw, columnMap, searchMap);
		buildOrderByQueryWrapper(qw, columnMap, orderBy);
		return qw;
	}

	/**
	 * 过滤带条件查询参数的Map
	 *
	 * @param paramMap
	 */
	private static Map<String, Object> filterConditionMap(Map<String, Object> paramMap) {
		Map<String, Object> conditionMap = null;
		if (CollectionUtil.isNotEmpty(paramMap)) {
			WrapperFilter filter = null;
			conditionMap = CollectionUtil.newHashMap();
			for (Map.Entry<String, Object> entry : paramMap.entrySet()) {
				filter = new WrapperFilter(entry.getKey(),
						// 匹配查询条件
						String.valueOf(entry.getValue()));
				if (filter.getMatchType() != null && StrUtil.isNotEmpty(String.valueOf(entry.getValue()))) {
					conditionMap.put(entry.getKey(), entry.getValue());
				}
			}
		}
		return conditionMap;
	}

	private static <T> void buildSearchQueryWrapper(QueryWrapper<T> qw, Map<String, String> columnMap, Map<String, Object> searchMap) {
		// 2、先过滤带有查询条件的参数
		Map<String, Object> conditionMap = filterConditionMap(searchMap);
		// 3、查询条件构建 eg. like_name_or_login_name
		if (CollectionUtil.isNotEmpty(conditionMap)) {
			WrapperFilter filter = null;
			int index = 0;
			for (Map.Entry<String, Object> entry : conditionMap.entrySet()) {
				filter = new WrapperFilter(entry.getKey(), String.valueOf(entry.getValue()));
				// 每次重新赋值
				index = 0;
				for (String propertyName : filter.getPropertyNames()) {
					// 判断是否在对应的key-value中，不在，那么直接跳过
					if (!columnMap.containsKey(propertyName)) {
						continue;
					}
					// 不是开始和结束字段
					if (index != 0 && index != filter.getPropertyNames().length) {
						qw.or();
					}
					switch (filter.getMatchType()) {
					case EQ:
						qw.eq(columnMap.get(propertyName), filter.getMatchValue());
						break;
					case NE:
						qw.ne(columnMap.get(propertyName), filter.getMatchValue());
						break;
					case LIKE:
						qw.like(columnMap.get(propertyName), String.valueOf(filter.getMatchValue()));
						break;
					case LT:
						qw.lt(columnMap.get(propertyName), filter.getMatchValue());
						break;
					case GT:
						qw.gt(columnMap.get(propertyName), filter.getMatchValue());
						break;
					case LE:
						qw.le(columnMap.get(propertyName), filter.getMatchValue());
						break;
					case GE:
						qw.ge(columnMap.get(propertyName), filter.getMatchValue());
						break;
					default:
						break;
					}
					index++;
				}
			}
		}
	}

	private static <T> void buildOrderByQueryWrapper(QueryWrapper<T> qw, Map<String, String> columnMap, Map<String, String> orderBy) {

		// 2、构 排序语句 eg. "userName:asc,userId:desc"
		if (MapUtil.isNotEmpty(orderBy)) {
			for (Map.Entry<String, String> entry : orderBy.entrySet()) {
				// 判断是否在对应的key-value中，不在，那么直接跳过
				if (!columnMap.containsKey(entry.getKey())) {
					continue;
				}
				if ("asc".equals(StringUtil.lowerCase(entry.getValue()))) {
					qw.orderByAsc(columnMap.get(entry.getKey()));
				} else if ("desc".equals(StringUtil.lowerCase(entry.getValue()))) {
					qw.orderByDesc(columnMap.get(entry.getKey()));
				}
			}
		}
	}
}

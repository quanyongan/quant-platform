package com.xinansoft.core.mybatis.base;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.LinkedCaseInsensitiveMap;

/**
 * 公共基类
 *
 * @author Quant
 * @date 2019/3/21 10:48
 */
@Getter
@Setter
public class BaseModel<T extends BaseModel<T>>  extends Model<T> {

    private static final long serialVersionUID = 2389240292287943823L;

    /**
     * 主键
     **/
    private Long id;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}

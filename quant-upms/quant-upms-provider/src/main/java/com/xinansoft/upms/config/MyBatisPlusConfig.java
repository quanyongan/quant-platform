package com.xinansoft.upms.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.extension.injector.LogicSqlInjector;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.xinansoft.core.mybatis.handler.QuantMetaObjectHandler;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * mybatis-plus配置相关
 *
 * @author Quant
 * @date 2019/3/21 10:36
 */
@EnableTransactionManagement
@Configuration
@MapperScan("com.xinansoft.upms.mapper")
public class MyBatisPlusConfig {

    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    /**
     * 逻辑删除注入器
     * @return
     */
    @Bean
    public ISqlInjector sqlInjector() {
        return new LogicSqlInjector();
    }


    /**
     * 自定义自动填充
     * @return
     */
    @Bean
    public MetaObjectHandler metaObjectHandler() {
        return new QuantMetaObjectHandler();
    }
}

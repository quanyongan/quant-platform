package com.xinansoft.upms.mapper;

import com.xinansoft.core.mybatis.base.BaseMapper;
import com.xinansoft.upms.model.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * 角色管理数据持久化接口
 *
 * @author Quant
 * @date 2019/3/20 14:26
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 根据角色编号查询所有的菜单编码
     *
     * @param userId 用户编号
     * @return List
     */
    Set<String> listCodeByRoleId(@Param("userId") Long userId);

}

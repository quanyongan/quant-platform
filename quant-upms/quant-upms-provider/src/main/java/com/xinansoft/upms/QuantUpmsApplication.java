package com.xinansoft.upms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * UPMS 权限管理系统启动服务
 *
 * @author Quant
 * @date 2018-08-13 18:22:52
 */
@SpringBootApplication(scanBasePackages = "com.xinansoft.**")
public class QuantUpmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuantUpmsApplication.class, args);
    }
}

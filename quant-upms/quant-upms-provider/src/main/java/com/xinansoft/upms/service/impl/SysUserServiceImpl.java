package com.xinansoft.upms.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xinansoft.core.mybatis.base.BaseServiceImpl;
import com.xinansoft.upms.mapper.SysRoleMapper;
import com.xinansoft.upms.mapper.SysRoleMenuMapper;
import com.xinansoft.upms.mapper.SysUserMapper;
import com.xinansoft.upms.model.SysUser;
import com.xinansoft.upms.service.SysUserService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;

/**
 * 用户管理服务实现
 *
 * @author Quant
 * @date 2019/3/20 14:31
 */
@Service(version = "1.0")
public class SysUserServiceImpl extends BaseServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    private SysUserMapper userMapper;
    @Autowired
    private SysRoleMapper roleMapper;
    @Autowired
    private SysRoleMenuMapper roleMenuMapper;

    @Override
    public Boolean changeStatus(SysUser user) {
        Assert.notNull(user.getId(), "用户编号不存在");
        userMapper.updateById(user);
        return true;
    }

    @Override
    public SysUser getByUserName(String userName) {

        SysUser user = userMapper.selectOne(new QueryWrapper<SysUser>(new SysUser(userName)));

        if (user == null) {
            return null;
        }
        // 查询用户所属的角色
        user.setRoles(roleMapper.listByUserId(user.getId()));
        // 查询用户所属角色对应的菜单权限
        user.setPerms(roleMenuMapper.listPermsByUserId(user.getId()));
        return user;
    }
}

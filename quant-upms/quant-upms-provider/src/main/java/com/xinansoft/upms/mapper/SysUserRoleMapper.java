package com.xinansoft.upms.mapper;

import com.xinansoft.core.mybatis.base.BaseMapper;
import com.xinansoft.upms.model.SysUserRole;

import java.util.List;

/**
 * 用户角色管理数据持久化接口
 *
 * @author Quant
 * @date 2018-08-15 09:40:24
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

    /**
     * 根据用户编号，查询出所有的所属的角色
     *
     * @param userId
     * @return
     */
    List<Long> listByUserId(Long userId);


    void deleteByRoleIds(Long[] roleIds);

}

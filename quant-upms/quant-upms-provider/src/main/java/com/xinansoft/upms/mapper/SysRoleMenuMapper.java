package com.xinansoft.upms.mapper;

import com.xinansoft.core.mybatis.base.BaseMapper;
import com.xinansoft.upms.model.SysRoleMenu;
import org.apache.ibatis.annotations.Param;

import java.util.Set;

/**
 * 角色菜单管理数据持久化接口
 * 
 * @author Quant
 * @date 2018-08-15 09:40:24
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

	/**
	 * 根据用户编号查询所属角色角色菜单权限
	 * 
	 * @param userId
	 *            用户编号
	 * @return Set
	 */
	Set<String> listPermsByUserId(@Param("userId") Long userId);

	/**
	 * 根据角色编号删除
	 * 
	 * @param roleIds
	 *            角色编号
	 * @return 受影响的行数
	 */
	Integer deleteByRoleIds(Long[] roleIds);

}

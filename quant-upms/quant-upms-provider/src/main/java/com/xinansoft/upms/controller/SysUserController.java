package com.xinansoft.upms.controller;

import com.xinansoft.core.common.log.annotation.SysLog;
import com.xinansoft.core.common.ui.Pagination;
import com.xinansoft.core.common.ui.Result;
import com.xinansoft.core.mybatis.base.BaseController;
import com.xinansoft.upms.model.SysUser;
import com.xinansoft.upms.service.SysUserService;
import io.swagger.annotations.Api;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

/**
 * 用户管理控制器
 *
 * @author Quant
 * @date 2019/3/12 9:30
 */
@Api(tags = "用户管理")
@RestController
@RequestMapping("/users")
public class SysUserController extends BaseController<SysUser> {

    @Reference(version = "1.0")
    private SysUserService sysUserService;

    @SysLog("获取用户信息")
    @GetMapping("/info/{userName}")
    @PreAuthorize("hasAuthority('user:info')")
    public Result getByUserName(@PathVariable("userName") String userName) {
        return Result.success(sysUserService.getByUserName(userName));
    }

    @SysLog("用户分页数据")
    @PostMapping("/page")
    public Result getPagination(@RequestBody Pagination pagination) {
        return Result.success(sysUserService.getPagination(pagination));
    }

    @SysLog("新增用户")
    @PostMapping
    public Result insert(@RequestBody SysUser user) {
        return Result.success(sysUserService);
    }

    @SysLog("修改用户")
    @PutMapping
    public Result update(@RequestBody SysUser user) {
        return Result.success(sysUserService.save(user));
    }

    @SysLog("删除用户")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable("id") Long id) {
        return Result.success(sysUserService.removeById(id));
    }

    @PostMapping("/view")
    @PreAuthorize("hasAuthority('user:view')")
    public Result view(Authentication authentication) {
        return Result.success(authentication);
    }
}

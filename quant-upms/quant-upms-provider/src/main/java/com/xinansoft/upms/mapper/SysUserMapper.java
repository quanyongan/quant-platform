package com.xinansoft.upms.mapper;

import com.xinansoft.core.mybatis.base.BaseMapper;
import com.xinansoft.upms.model.SysUser;

/**
 * 用户管理数据持久化接口
 *
 * @author Quant
 * @date 2019/3/21 10:52
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}

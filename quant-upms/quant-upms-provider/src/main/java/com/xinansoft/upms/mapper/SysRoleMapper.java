package com.xinansoft.upms.mapper;

import com.xinansoft.core.mybatis.base.BaseMapper;
import com.xinansoft.upms.model.SysRole;
import org.apache.ibatis.annotations.Param;

import java.util.Set;

/**
 * 角色管理数据持久化接口
 *
 * @author Quant
 * @date 2018-08-15 09:40:24
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

	/**
	 * 根据用户编号查询所属角色列表
	 *
	 * @param userId 用户编号
	 * @return List
	 */
	Set<String> listByUserId(@Param("userId") Long userId);

}

package com.xinansoft.upms.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.xinansoft.core.mybatis.base.AutoFillModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * 角色类
 *
 * @author Quant
 * @date 2019/3/12 9:24
 */
@Getter
@Setter
@NoArgsConstructor
@TableName("sys_role")
public class SysRole extends AutoFillModel<SysRole> {

	private static final long serialVersionUID = -598740537545293634L;

	/** 角色名 */
	private String name;

	/** 角色编码 */
	private String code;

	/** 应用码 */
	private String appCode;

	/** 父角色id */
	private Long parentId;

	/** 备注 */
	private String description;

	/** 父角色 */
	@TableField(exist=false)
	private SysRole parentRole;

	@TableField(exist=false)
	private List<SysRole> children;

}

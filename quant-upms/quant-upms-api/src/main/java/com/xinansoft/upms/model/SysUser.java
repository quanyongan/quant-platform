package com.xinansoft.upms.model;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.xinansoft.core.mybatis.base.AutoFillModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

/**
 * 用户管理
 *
 * @author Quant
 * @date 2019/3/12 9:25
 */
@Getter
@Setter
@NoArgsConstructor
@TableName("sys_user")
public class SysUser extends AutoFillModel<SysUser> {

	private static final long serialVersionUID = -4826026759047607054L;

	/**
	 * 真实姓名
	 */
	private String realName;
	/**
	 * 用户登录名
	 */
	private String userName;

	/**
	 * 密码
	 */
	private String password;
	/**
	 * 性别
	 */
	private Integer sex;

	/**
	 * 邮箱
	 */
	private String email;

	/**
	 * 手机号
	 */
	private String mobile;

	/**
	 * 状态 0：禁用 1：正常
	 */
	private Integer status;

	/**
	 * 是否管理员
	 */
	private Integer admin;

	/**
	 * 备注
	 */
	private String description;

	/**
	 * 角色
	 */
	@TableField(exist = false)
	private Set<String> roles;

	/**
	 * 权限
	 */
	@TableField(exist = false)
	private Set<String> perms = CollUtil.newHashSet();

	public SysUser(String userName) {
		this.userName = userName;
	}

}

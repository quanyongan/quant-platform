package com.xinansoft.upms.service;


import com.xinansoft.core.mybatis.base.BaseService;
import com.xinansoft.upms.model.SysUser;

/**
 * 用户服务接口
 *
 * @author Quant
 * @date 2019/3/12 9:25
 */
public interface SysUserService extends BaseService<SysUser> {

    /**
     * 更改状态
     *
     * @param user 实体
     * @return Boolean
     */
    Boolean changeStatus(SysUser user);


    /**
     * 根据用户名获取用户的信息
     *
     * @param userName
     */
    SysUser getByUserName(String userName);

}

package com.xinansoft.upms.model;

import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import com.xinansoft.core.mybatis.base.AutoFillModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 组织架构
 *
 * @author Quant
 * @date 2019/3/12 9:24
 */
@Getter
@Setter
@NoArgsConstructor
@TableName("sys_org")
public class SysOrg extends AutoFillModel<SysOrg> {

	private static final long serialVersionUID = 1119274126992346298L;

	/**
	 * 上级组织
	 */
	private Long parentId;

	/**
	 * 组织代码
	 */
	private String code;

	/**
	 * 组织名称
	 */
	private String name;

	/**
	 * 备注
	 */
	private String description;

	/**
	 * 父级组织
	 */
	@TableField(exist = false)
	private SysOrg parentOrg;

	/**
	 * 子级组织
	 */
	@TableField(exist = false)
	private List<SysOrg> children;

}

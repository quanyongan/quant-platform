package com.xinansoft.upms.model;

import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import com.xinansoft.core.mybatis.base.AutoFillModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 菜单类
 *
 * @author Quant
 * @date 2019/3/12 9:24
 */
@TableName("sys_menu")
@Getter
@Setter
@NoArgsConstructor
public class SysMenu extends AutoFillModel<SysMenu> {

	private static final long serialVersionUID = -5018857743172669971L;

	/**
	 * 菜单名 
	 */
	private String name;
	/**
	 * 权限标识 
	 */
	private String perms;

	/** 
	 * 应用编码
	 */
	private String appCode;

	/**
	 * 父菜单id
	 */
	private Integer parentId;

	/**
	 *  菜单地址
	 */
	private String url;

	/**
	 *  菜单iconClass
	 */
	private String icon;
	
	/**
	 * 类型（0：目录，1：菜单，2：按钮）
	 */
	private Integer type;

	/**
	 * 状态
	 */
	private Integer status;

	/**
	 * 层级路径
	 */
	private String path;

	/**
	 * 备注
	 */
	private String description;

	/**
	 * 子级菜单
	 */
	@TableField(exist = false)
	private List<SysMenu> children;

}

package com.xinansoft.upms.service;

import com.xinansoft.core.mybatis.base.BaseService;
import com.xinansoft.upms.model.SysRole;

/**
 * 角色服务接口
 *
 * @author Quant
 * @date 2019/3/12 9:25
 */
public interface SysRoleService extends BaseService<SysRole> {

	/**
	 * 更改状态
	 * 
	 * @param role
	 *            实体
	 * @return Boolean
	 */
	Boolean changeStatus(SysRole role);
}

package com.xinansoft.upms.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xinansoft.core.mybatis.base.BaseModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 用户角色管理
 *
 * @author Quant
 * @date 2019/3/12 9:25
 */
@Getter
@Setter
@NoArgsConstructor
@TableName("sys_user_role")
public class SysUserRole extends BaseModel<SysUserRole> {

	private static final long serialVersionUID = -3939177219723410690L;

	/**
	 * 用户编号
	 */
	private Long userId;

	/**
	 * 角色编号
	 */
	private Long roleId;

	public SysUserRole(Long userId) {
		this.userId = userId;
	}

}

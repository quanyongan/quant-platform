package com.xinansoft.upms.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xinansoft.core.mybatis.base.BaseModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 角色菜单管理
 *
 * @author Quant
 * @date 2019/3/12 9:25
 */
@Getter
@Setter
@NoArgsConstructor
@TableName("sys_role_menu")
public class SysRoleMenu extends BaseModel<SysRoleMenu> {

	private static final long serialVersionUID = -4881911365711863110L;

	/**
	 * 角色编号
	 */
	private Long roleId;

	/**
	 * 菜单编号
	 */
	private Long menuId;

}
